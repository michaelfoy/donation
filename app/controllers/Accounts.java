package controllers;

import play.*;
import play.mvc.*;
import java.util.*;
import models.*;

/**
 * @file Accounts.java
 * @brief Controller for home page
 * @author michaelfoy
 * @version 2016-06-01
 */
public class Accounts extends Controller {

  /**
   * Method to open sign up page
   */
  public static void signup()  {
    render();
  }
  
  /**
   * Method to register a new user
   * @param user New user
   */
  public static void register(User user)  {
    if (user.usaCitizen && user.conditions)  {
      Logger.info("New user: " + user.firstName + " " + user.lastName + " Email: " + user.email
        + " Password: " + user.password);
      user.save();
      login();
    }  else  {
      Logger.info("Sign up of " + user.firstName + " " + user.lastName + " failed: Conditions not met");
      check();
    }
  }
  
  /**
   * Method to open check page
   */
  public static void check()  {
    render();
  }
  
  /**
   * Method to open log in page
   */
  public static void login()  {
    render();
  }
  
  /**
   * Method to log out current user
   */
  public static void logout()  {
    User user = getCurrentUser();
    Logger.info(user.firstName + " " + user.lastName + " logging out...");
    session.clear();
    render();
  }
  
  /**
   * Checks that email and password combination are correct
   * @param email Inputted email address
   * @param password Inputted password
   */
  public static void authenticate(String email, String password)  {
    Logger.info("Attempting to authenticate with: " + email + ", " + password);
    
    User user = User.findByEmail(email);
    if ((user != null) && (user.checkPassword(password)))  {
      Logger.info("Successful authentication of " + user.firstName + " " + user.lastName);
      session.put("logged_in_userid", user.id);
      DonationController.index();
    }  else  {
      Logger.info("Authentication failed");
      login();
    }    
  }
  
  /**
   * If a user is logged in, returns that user
   * @return The currently logged in user
   */
  public static User getCurrentUser()  {
    String userId = session.get("logged_in_userid");
    if (userId == null)  {
      return null;
    }
    User currentUser = User.findById(Long.parseLong(userId));
    Logger.info("Logged in user is " + currentUser.firstName + " " + currentUser.lastName);
    return currentUser;
  }

}
