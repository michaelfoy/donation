package controllers;

import play.*;
import play.mvc.*;
import models.*;

/**
 * @file Welcome.java
 * @brief Controller for landing page
 * @author michaelfoy
 * @version 2016-06-01
 */
public class Welcome extends Controller  {
  
  public static void index()  {
    Logger.info("Landed in Welcome class");
    render();
  }

}

