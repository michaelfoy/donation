package controllers;

import play.*;
import play.mvc.*;
import java.util.*;
import models.*;
import java.util.ArrayList;

/**
 * @file DonationController.java
 * @brief Controller for donation pages and associated methods
 * @author michaelfoy
 * @version 2016-06-01
 */
public class DonationController extends Controller{
  
  /**
   * Opens default donation page
   */
  public static void index()  {
    User user = Accounts.getCurrentUser();
    int progress = Donation.progress();
    render(user, progress);
  }
  
  /**
   * Opens donation issue page
   */
  public static void donationIssue()  {
    render();
  }
  
  /**
   * Opens donation completion page
   */
  public static void donationComplete()  {
    render();
  }
  
  /**
   * Opens donation report page
   */
  public static void report()  {
    ArrayList<Donation> donations = Donation.getDonations();
    render(donations);
  }
  
  /**
   * Creates and saves a new donation
   * @param id Id of the currently logged in user
   * @param amount The sum of the donation
   * @param method The method of payment
   */
  public static void donate(Long id, int amount, String method)  {
    if((method == null) || (amount == 0))  {
      donationIssue();
    }  else  {
    User benefactor = User.findById(id);
    Donation donation = new Donation(benefactor, amount, method);
    Logger.info(benefactor.firstName + " " + benefactor.lastName + " has donated " + amount + " by " + method);
    Logger.info("Current fund: " + donation.progress());
    donation.save();
    donationComplete();
    }
  }

}
