package models;

import play.db.jpa.Model;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.ArrayList;
/**
 * @file Donation.java
 * @brief Models a donation from a logged in user and relative methods
 * @author michaelfoy
 * @version 2016-06-01
 */
@Entity
public class Donation extends Model{
  
  @ManyToOne
  public User benefactor;
  
  public int amount;
  public String methodDonated;
  public static final double TARGET = 50000;
  public static double currentFund = 0;
  private static ArrayList<Donation> donations = new ArrayList<Donation>();
  
  /**
   * Constructs a donation object
   * @param benefactor User giving the donation
   * @param amount The sum of the donation
   * @param methodDonated The method of donation
   */
  public Donation (User benefactor, int amount, String methodDonated)  {
    this.benefactor = benefactor;
    this.amount = amount;
    this.methodDonated = methodDonated;
    currentFund += amount;
    donations.add(this);
  }
  /**
   * Returns the percentage of fund relevant to target
   * @return The percentage of completion
   */
  public static int progress()  {
    double val = (currentFund / TARGET) * 100;
    return (int)val;
  }
  
  /**
   * Returns arraylist of all donations
   * @return arraylist of all donations
   */
  public static ArrayList<Donation> getDonations()  {
    return donations;
  }
}
