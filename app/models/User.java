package models;

import play.db.jpa.Model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;

/**
 * @file User.java
 * @brief Class which describes a user
 * @author michaelfoy
 * @version 2016-06-01
 */
@Entity
public class User extends Model {
	public String email;
	public String firstName;
	public String lastName;
	public String password;
	public boolean usaCitizen = false;
	public boolean conditions = false;
	
  @OneToMany(mappedBy = "benefactor")
  public List<Donation> contributions = new ArrayList<Donation>();
	/**
	 * Constructs a User object
	 * @param usaCitizen True if user is a US citizen
	 * @param firstName User's first name
	 * @param lastName User's last name
	 * @param email User's email
	 * @param password User's password
	 */
	public User (boolean usaCitizen, boolean conditions, String firstName, String lastName, String email, String password)  {
	  this.usaCitizen = usaCitizen;
	  this.conditions = conditions;
	  this.firstName = firstName;
	  this.lastName = lastName;
	  this.email = email;
	  this.password = password;
	}

	/**
	 * Finds a user using their email address
	 * @param email The email address of the user being searched
	 * @return The relevant user for the email address
	 */
	public static User findByEmail(String email)  {
	  return find("email", email).first();
	}
	
	/**
	 * Checks if a given password matches a User's password
	 * @param password The password to be checked
	 * @return True if the password is correct for this user
	 */
	public boolean checkPassword(String password)  {
	  return this.password.equals(password);
	}
}
