import java.io.*;
import play.*;
import play.jobs.*;
import play.libs.MimeTypes;
import play.test.*;

import models.*;
import play.db.jpa.Blob;
import models.User;

@OnApplicationStart
public class Bootstrap extends Job 
{ 
  public void doJob() throws FileNotFoundException
  {
    Fixtures.deleteDatabase();
    Fixtures.loadModels("data.yml");
  }
}
